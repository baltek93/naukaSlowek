package com.baltek.mistrzslow.data.Model

import io.realm.RealmList
import io.realm.RealmModel
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import io.realm.annotations.RealmClass

/**
 * Created by Bartosz Kołtuniak on 06.11.2016.
 */


open class Categories(@PrimaryKey
                 var id: Long = 1,
                 var name: String? = null,
                 var wordPLs: RealmList<WordPL>? = null,
                 var url: String? = null) : RealmObject()