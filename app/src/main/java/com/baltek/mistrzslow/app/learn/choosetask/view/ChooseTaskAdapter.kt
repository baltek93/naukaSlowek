package com.baltek.mistrzslow.app.learn.choosetask.view

import android.content.Context
import android.support.constraint.ConstraintLayout
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

import com.baltek.mistrzslow.R
import com.baltek.mistrzslow.app.learn.choosetask.presenter.ChooseTaskPresenter
import com.baltek.mistrzslow.app.learn.choosetask.view.ChooseTaskAdapter.Holder

class ChooseTaskAdapter(private val mContext: Context, private val name: List<String>, private val presenter: ChooseTaskPresenter)//this.categories = categories;
    : RecyclerView.Adapter<Holder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.list_item_choose_task, parent, false)

        return Holder(itemView)
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {

        // int imageResource = R.drawable.icon;

        holder.chooseBackgroud!!.setOnClickListener{
            when (position) {
                0 -> presenter.renderFlashCard()
                1 -> presenter.renderAdjust()
                2 -> presenter.renderListen()
            }
        }
        holder.name!!.text = name[position]
    }

    override fun getItemCount(): Int {
        return name.size
    }

    inner class Holder(view: View) : RecyclerView.ViewHolder(view) {
        internal var name: TextView = view.findViewById(R.id.name_task)
        internal var chooseBackgroud: ConstraintLayout = view.findViewById(R.id.choose_item_background_CL)

    }
}

