package com.baltek.mistrzslow.app.search.view

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

import com.baltek.mistrzslow.data.Model.WordEN
import com.baltek.mistrzslow.data.Model.WordPL
import com.baltek.mistrzslow.R
import com.baltek.mistrzslow.app.search.view.WordAdapterEN.Holder

/**
 * Created by Bartosz Kołtuniak on 05.12.2016.
 */

class WordAdapterEN(private val mContext: Context?, private val wordPLs: List<WordPL>, private val wordsEN: List<WordEN>) : RecyclerView.Adapter<Holder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.list_item_search_words, parent, false)

        return Holder(itemView)
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        val word = wordsEN[position]
        val wordPL: Array<String>? = null
        holder.textWordEN!!.text = word.name
        val stringBuilder = StringBuilder()
        var amount = 0
        for (w in wordPLs) {
            for (wEN in w.wordEN!!) {
                if (wEN.id == word.id) {
                    if (amount == 0) {
                        stringBuilder.append(w.name)
                        amount++
                    } else {
                        stringBuilder.append("\n" + w.name!!)

                    }

                }
            }
        }
        holder.textWordPL!!.text = stringBuilder.toString()

        //holder.overflow.setOnClickListener(new View.OnClickListener() {
        //    @Override
        //    public void onClick(View view) {
        //        showPopupMenu(holder.overflow);
        //    }
        //});
    }

    override fun getItemCount(): Int {
        return wordsEN.size
    }

    inner class Holder(view: View) : RecyclerView.ViewHolder(view) {
        val textWordPL: TextView = view.findViewById(R.id.polish_word)
        val textWordEN: TextView = view.findViewById(R.id.english_word)

    }
}
