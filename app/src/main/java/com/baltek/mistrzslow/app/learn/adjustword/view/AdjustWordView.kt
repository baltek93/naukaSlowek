package com.baltek.mistrzslow.app.learn.adjustword.view

import com.baltek.mistrzslow.app.base.BaseView
import com.baltek.mistrzslow.data.Model.WordPL

interface AdjustWordView : BaseView {
    fun renderQuestion(wordPLs: List<WordPL>)
}
