package com.baltek.mistrzslow.app.search.presenter

import com.baltek.mistrzslow.app.base.Presenter
import com.baltek.mistrzslow.app.search.usecase.SearchUseCase
import com.baltek.mistrzslow.app.search.view.SearchView
import com.baltek.mistrzslow.data.Model.WordPL
import io.reactivex.functions.Consumer
import java.io.InputStream

/**
 * Created by Bartosz Kołtuniak on 02.12.2016.
 */

class SearchDetailsPresenter(private var useCase: SearchUseCase) : Presenter<SearchView>(useCase) {

    override fun onViewVisible() {
        if (getView() != null) {


            try {
                query(useCase.saveAll(getView().getMainActivity().assets.open("words.json")).subscribe { t: Boolean? ->
                    run {
                        if (t!!) {
                            query(useCase.getAllWords().subscribe { wordsAll: List<WordPL>? -> getAllWords(wordsAll!!) })
                        }
                    }
                })
            } catch (e: Exception) {

            }
        }
//            try {
//                query(useCase.saveAll((InputStream) getView().getActivity().getAssets().open("words.json")).subscribe(aBoolean -> {
//                    if (aBoolean) {
//                        query(useCase.getAllWords().subscribe(categories -> {
//                            getAllWords(categories);
//                        }));
//                    }
//                }));
//            } catch (IOException e) {
//            }
    }
    //  @Override
    //  public void onViewVisible() {
    //    if(getView()!=null)
    //    {
    //
    //      try {
    //        query(useCase.saveAll((InputStream) getView().getActivity().getAssets().open("words.json")).subscribe(aBoolean -> {
    //          if(aBoolean)
    //          {
    //            query(useCase.getAllWords().subscribe(categories -> {
    //              getAllWords(categories);
    //            }));
    //          }
    //        }));
    //      } catch (IOException e) {
    //      }
    //    }
    //  }

    fun getAllWords(categories: List<WordPL>) {
        if (getView() != null) {
            getView().getAllWords(categories)
        }
    }

    fun getWordsPL(string: String) {
        if (getView() != null) {
            query(useCase.getWordPL(string).subscribe { t -> getView().getWordsPL(wordPLs = t) })
        }
    }

    fun getWordsEN(string: String) {
        if (getView() != null) {
            query(useCase.getWordEN(string).subscribe { t -> getView().getWordsEN(t) })
        }
    }
}
