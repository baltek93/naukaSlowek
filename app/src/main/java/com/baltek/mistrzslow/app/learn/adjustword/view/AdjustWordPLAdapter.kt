package com.baltek.mistrzslow.app.learn.adjustword.view

import android.content.Intent
import android.support.v4.app.Fragment
import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

import com.baltek.mistrzslow.R
import com.baltek.mistrzslow.app.base.Navigator
import com.baltek.mistrzslow.app.learn.adjustword.view.AdjustWordPLAdapter.Holder
import com.baltek.mistrzslow.data.Model.WordEN
import com.baltek.mistrzslow.data.Model.WordPL

import java.util.ArrayList

class AdjustWordPLAdapter(private var wordPLList: ArrayList<WordPL>, internal var fragment: Fragment,
                           var wordENList: ArrayList<WordEN>,  var enList: RecyclerView,  wordENAdapter: AdjustWordENAdapter, var navigator: Navigator) : RecyclerView.Adapter<Holder>(), View.OnClickListener {
    internal var isvisible = ArrayList<Boolean>()
    internal var isvisibleEN: ArrayList<Boolean>
    internal var theEnd = false
    private var focusedItem = -1

    init {
        for (i in wordPLList.indices)
            isvisible.add(true)
        isvisibleEN = wordENAdapter.isVisible
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.list_item_adjust, parent, false)

        return Holder(itemView)
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.textWordPL!!.text = wordPLList[position].name
        holder.backgroud!!.setTag(R.string.id_visible_recyclerview, isvisible[position])
        if (!isvisible[position]) {
            holder.backgroud!!.visibility = View.INVISIBLE
        }
        for (i in wordENList.indices) {
            val enView = enList.layoutManager.findViewByPosition(i)
            val backgroundEN = enView.findViewById<View>(R.id.categories_background) as CardView
            if (backgroundEN.getTag(R.string.id_selected_recyclerview) == "selected") {
                val textEn = enView.findViewById<View>(R.id.wordTV) as TextView
                if (textEn.text
                                .toString() == wordPLList[position].wordEN!![0].name) {
                    isvisible[position] = false
                    isvisibleEN[i] = false
                    backgroundEN.visibility = View.INVISIBLE
                    backgroundEN.setTag(R.string.id_selected_recyclerview, "noselected")
                    backgroundEN.setTag(R.string.id_visible_recyclerview, isvisible[position])
                    holder.backgroud!!.visibility = View.INVISIBLE
                    holder.backgroud!!.setTag(R.string.id_visible_recyclerview, isvisible[position])
                    holder.backgroud!!.setTag(R.string.id_selected_recyclerview, "noselected")
                }
                if (!isvisible[i]) {
                    theEnd = true
                }

            }
        }
        holder.backgroud!!.setOnClickListener { view ->
            if (focusedItem != -1) if (isvisible[focusedItem]) notifyItemChanged(focusedItem)

            focusedItem = holder.layoutPosition
            notifyItemChanged(focusedItem)
        }

        if (holder.backgroud!!.getTag(R.string.id_visible_recyclerview) == true) {
            if (focusedItem == position) {
                holder.backgroud!!.setCardBackgroundColor(
                        fragment.resources.getColor(R.color.primary900))
                holder.backgroud!!.setTag(R.string.id_selected_recyclerview, "selected")
            } else {
                holder.backgroud!!.setCardBackgroundColor(
                        fragment.resources.getColor(R.color.primary300))
                holder.backgroud!!.setTag(R.string.id_selected_recyclerview, "noselected")
            }
        }
        for (i in isvisible.indices) {
            if (isvisible[i]) {
                theEnd = false
            }
        }
        if (theEnd) {
            //todo navigator
            fragment.activity!!.onBackPressed()
//            fragment.startActivity(Intent(fragment.context, LearnActivity::class.java))
//            fragment.activity!!.finish()
        }
    }

    override fun getItemCount(): Int {
        return 8
    }

    override fun onClick(view: View) {}

    inner class Holder(view: View) : RecyclerView.ViewHolder(view) {
        internal var textWordPL: TextView = view.findViewById(R.id.wordTV)
        internal var backgroud: CardView = view.findViewById(R.id.categories_background)

    }
}

