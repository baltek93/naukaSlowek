package com.baltek.mistrzslow.app.base

import android.app.Activity

interface BaseView {
    fun showToast(message: String)

    fun log(message: String)

    fun getStringFromId(id: Int): String

    fun getMainActivity(): Activity

    fun showDialog(s: String)
}