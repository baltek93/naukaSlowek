package com.baltek.mistrzslow.app.learn.choosetask.view;

import com.baltek.mistrzslow.app.base.BaseView;

public interface ChooseTaskView extends BaseView {
  void renderListen();

  void renderFlashCard();

  void renderAdjust();
}
