package com.baltek.mistrzslow.app.base


import android.app.Activity
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import org.kodein.di.KodeinAware
import org.kodein.di.KodeinContext
import org.kodein.di.erased.kcontext
import timber.log.Timber
import org.kodein.di.android.closestKodein
import org.kodein.di.erased.factory
import org.kodein.di.erased.instance

abstract class BaseFragment<PRESENTER : Presenter<VIEW>, VIEW : BaseView> : BaseView, Fragment(), KodeinAware {

    val drawerManager: DrawerManager by instance()

    //    private Unbinder mUnbinder;

    override val kodeinContext: KodeinContext<*> get() = kcontext(activity)

    override val kodein by closestKodein()

    abstract val layoutId: Int
    val navigator:Navigator  by instance(activity)
    protected abstract val presenter: PRESENTER?

    open val menuSettings: SetMenu
        get() = SetMenu.NONE

    override fun getStringFromId(id: Int): String {
        return getString(id)
    }

    override fun showDialog(message: String) {
        //      Util.showDialog(getFragmentManager(), MessageDialogFragment.newInstance(message));
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initInjection()

    }

    override fun getMainActivity(): Activity {
        return activity as Activity
    }

    //called in on create
    protected abstract fun initInjection()

    override fun onResume() {
        super.onResume()
        presenter!!.onViewVisible()
    }

    override fun onPause() {
        super.onPause()
        presenter!!.onViewHidden()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        var view: View? = null
        //      if (getMenuSettings() == SetMenu.NONE) {
        // kontenery
//        view = inflater.inflate(layoutId, container, false)
        //        mUnbinder = ButterKnife.bind(this, view);
//        return view
        //      }
        //      if (getMenuSettings() == SetMenu.LEFT) {
        val view1 = drawerManager.inflateMenu(inflater, layoutId)
        //        mUnbinder = ButterKnife.bind(this, view1);
        return view1
        //      }

        //      return view;
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        presenter!!.onViewCreated(this as VIEW)
    }

    override fun onDestroyView() {
        if (presenter != null) presenter!!.onViewDestroyed()
        super.onDestroyView()
        //      if (mUnbinder != null) mUnbinder.unbind();
    }

    override fun showToast(message: String) {
        Toast.makeText(activity, message, Toast.LENGTH_LONG).show()
    }

    override fun log(message: String) {
        Timber.d(message)
    }

    enum class SetMenu {
        LEFT, NONE
    }
}
