package com.baltek.mistrzslow.app.learn.choosetask.presenter

import com.baltek.mistrzslow.app.base.Presenter
import com.baltek.mistrzslow.app.learn.choosetask.usecase.ChooseTaskUseCase
import com.baltek.mistrzslow.app.learn.choosetask.view.ChooseTaskView

class ChooseTaskPresenter(private var useCase: ChooseTaskUseCase) : Presenter<ChooseTaskView>(useCase) {

    fun renderListen() {
        getView().renderListen()
    }

    fun renderFlashCard() {
        getView().renderFlashCard()
    }

    fun renderAdjust() {
        getView().renderAdjust()
    }
}
