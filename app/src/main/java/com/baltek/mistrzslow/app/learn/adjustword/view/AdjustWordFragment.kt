package com.baltek.mistrzslow.app.learn.adjustword.view

import android.os.Bundle
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.TextView

import com.baltek.mistrzslow.R
import com.baltek.mistrzslow.app.base.BaseFragment
import com.baltek.mistrzslow.app.learn.adjustword.presenter.AdjustWordPresenter
import com.baltek.mistrzslow.data.Model.WordEN
import com.baltek.mistrzslow.data.Model.WordPL

import java.util.ArrayList
import java.util.Collections

import timber.log.Timber

import com.baltek.mistrzslow.app.learn.choosetask.view.ChooseTaskFragment.Companion.CATEGORIESID
import kotlinx.android.synthetic.main.fragment_adjust_word.*
import kotlinx.android.synthetic.main.toolbar.*
import org.kodein.di.erased.instance

class AdjustWordFragment : BaseFragment<AdjustWordPresenter, AdjustWordView>(), AdjustWordView {
    override val presenter: AdjustWordPresenter? by instance()
    lateinit var wordPLList: ArrayList<WordPL>
    lateinit var wordENList: ArrayList<WordEN>
    lateinit var wordENAdapter: AdjustWordENAdapter
    lateinit var wordPLAdapter: AdjustWordPLAdapter

    override val layoutId: Int
        get() = R.layout.fragment_adjust_word

    override fun initInjection() {
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        drawerManager.inflateToolbar(toolbar, R.layout.toolbar_content_text_back, this.requireActivity())
//        (ButterKnife.findById(toolbar, R.id.toolbar_title) as TextView).text = "Dopasuj Słówka"
        val mLayoutManager = LinearLayoutManager(context)
        val mLayoutManager1 = LinearLayoutManager(context)
        wordENRV.layoutManager = mLayoutManager
        wordENRV.itemAnimator = DefaultItemAnimator()
        wordPLRV.layoutManager = mLayoutManager1
        wordPLRV.itemAnimator = DefaultItemAnimator()
        presenter!!.setCategoriesId(arguments!!.getLong(CATEGORIESID))
    }

    override fun renderQuestion(wordPLs: List<WordPL>) {
        wordPLList = ArrayList()
        wordENList = ArrayList()
        for (wordPL in wordPLs) {
            if (wordPLList.size < 8) {
                wordPLList.add(wordPL)
                wordENList.add(wordPL.wordEN!![0])

                Timber.e(wordENList.size.toString())
                Timber.e(wordPLList.size.toString())
            }
        }
        wordENList.shuffle()
        wordPLList.shuffle()
        wordENAdapter = AdjustWordENAdapter(wordENList, context!!, wordPLList)
        wordENRV.adapter = wordENAdapter

        wordPLAdapter = AdjustWordPLAdapter(wordPLList, this, wordENList, wordENRV, wordENAdapter,navigator)
        wordPLRV.adapter = wordPLAdapter
    }

    companion object {

        fun newInstance(categoriesId: Long?): AdjustWordFragment {
            val args = Bundle()
            val fragment = AdjustWordFragment()
            args.putLong(CATEGORIESID, categoriesId!!)
            fragment.arguments = args
            return fragment
        }
    }
}
