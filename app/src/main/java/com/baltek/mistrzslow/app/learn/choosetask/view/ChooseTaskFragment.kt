package com.baltek.mistrzslow.app.learn.choosetask.view

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.TextView

import com.baltek.mistrzslow.R
import com.baltek.mistrzslow.app.base.BaseFragment
import com.baltek.mistrzslow.app.learn.adjustword.view.AdjustWordFragment
import com.baltek.mistrzslow.app.learn.choosetask.presenter.ChooseTaskPresenter
import com.baltek.mistrzslow.app.learn.flashcards.view.FlashCardFragment
import com.baltek.mistrzslow.app.learn.listenwrite.view.ListenWriteFragment
import kotlinx.android.synthetic.main.fragment_choose_task.*
import kotlinx.android.synthetic.main.toolbar.*
import org.kodein.di.erased.instance

import java.util.ArrayList

class ChooseTaskFragment : BaseFragment<ChooseTaskPresenter, ChooseTaskView>(), ChooseTaskView {
    override val presenter: ChooseTaskPresenter? by instance()
    override val layoutId: Int
        get() = R.layout.fragment_choose_task

    override val menuSettings: BaseFragment.SetMenu
        get() = BaseFragment.SetMenu.LEFT

    override fun initInjection() {}

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        drawerManager.inflateToolbar(toolbar, R.layout.toolbar_content_text_back, this.requireActivity())
        toolbar.findViewById<TextView>(R.id.toolbar_title).text = "Wybierz Zadanie"

        val mLayoutManager = LinearLayoutManager(context)
        choose_RV!!.layoutManager = mLayoutManager
        val name: MutableList<String> = arrayListOf()
        name.add("Fiszki")
        name.add("Dopasuj")
        name.add("Sluchaj i napisz")
        choose_RV!!.adapter = ChooseTaskAdapter(context!!, name, presenter!!)
    }

    override fun renderListen() {
        navigator!!.changeFragment(ListenWriteFragment.newInstance(arguments!!.getLong(CATEGORIESID)))
    }

    override fun renderFlashCard() {
        navigator!!.changeFragment(FlashCardFragment.newInstance(arguments!!.getLong(CATEGORIESID)))
    }

    override fun renderAdjust() {
        navigator!!.changeFragment(AdjustWordFragment.newInstance(arguments!!.getLong(CATEGORIESID)))

    }

    companion object {
        const val CATEGORIESID = "CATEGORIESID"

        fun newInstance(categorieId: Long): ChooseTaskFragment {
            val args = Bundle()
            val fragment = ChooseTaskFragment()
            args.putLong(CATEGORIESID, categorieId)
            fragment.arguments = args
            return fragment
        }
    }
}
