package com.baltek.mistrzslow.app.base

import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import org.joda.time.format.DateTimeFormat
import org.reactivestreams.Subscription
import java.util.ArrayList


open class Presenter<V : BaseView>(private val useCase: UseCase) {
    private var mUseCaseList: MutableList<UseCase> = ArrayList()
    private var mDisposable = CompositeDisposable()
    private var view: V? = null
    protected val presenter: Presenter<*>
        get() = this

    init {
        addAndReturnUseCase(useCase)
    }

    fun onViewCreated(view: V) {
        this.view = view
        onViewAttached()
    }


    open fun getView(): V {
        return view as V
    }

    fun <T : UseCase> addAndReturnUseCase(useCase: T): T {
        mUseCaseList.add(useCase)
        return useCase
    }

    fun onViewDestroyed() {
        if (mDisposable.isDisposed) {
            mDisposable.clear()
        }
        view = null
    }

    open fun onViewAttached() {

    }


    fun getStringFromId(id: Int): String {
        return if (view != null) {
            view!!.getStringFromId(id)
        } else {
            ""
        }
    }

    fun mainUpdate() {

    }

    open fun onViewVisible() {

    }

    open fun onViewHidden() {

    }


    protected fun query(disposable: Disposable) {
        mDisposable.add(disposable)
    }

    fun parseData(date: String): String {
        return "Valid through: " + DateTimeFormat.forPattern("M/d/yy").print(java.lang.Long.parseLong(date))
    }

    enum class ImageState {
        FIT_PRESERVE_SCALE, FIT
    }

}
