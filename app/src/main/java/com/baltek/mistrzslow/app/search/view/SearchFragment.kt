package com.baltek.mistrzslow.app.search.view

import android.Manifest
import android.app.Activity
import android.content.pm.PackageManager
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.util.Log
import android.view.View
import com.baltek.mistrzslow.R
import com.baltek.mistrzslow.R.drawable.english_flag

import com.baltek.mistrzslow.app.base.BaseFragment
import com.baltek.mistrzslow.app.search.presenter.SearchPresenter
import com.baltek.mistrzslow.data.Model.WordEN
import com.baltek.mistrzslow.data.Model.WordPL
import kotlinx.android.synthetic.main.fragment_search.*
import kotlinx.android.synthetic.main.toolbar.*
import org.kodein.di.erased.instance



class SearchFragment : BaseFragment<SearchPresenter, SearchView>(), SearchView {


    override val presenter: SearchPresenter?
            by instance()


    //    @BindDrawable(R.drawable.poland_flag)
//    internal var polish_flag: Drawable? = null
//    @BindDrawable(R.drawable.english_flag)
//    internal var english_flag: Drawable? = null
//    @BindView(R.id.toolbar)
//    internal var toolbar: Toolbar? = null
    //@BindView(R.id.wordsList) RecyclerView listWords;
//    @BindView(R.id.search_tablayout)
//    internal var mSearchTabLayout: TabLayout? = null
//    internal var search_editText: EditText? = null
//    private val wordAdapter: com.bartoszkoltuniak.pracainz.app.search.view.WordAdapterPL? = null
//    @Inject
//    override var presenter: SearchPresenter? = null
//        internal set
    private val wordPLs: List<WordPL>? = null

    override val layoutId: Int
        get() = R.layout.fragment_search

    override val menuSettings: BaseFragment.SetMenu
        get() = BaseFragment.SetMenu.LEFT

    override fun getAllWords(categories: List<WordPL>) {
        //wordPLs = categories.get(0).getWordPLs();
        //wordAdapter = new WordAdapterPL(getContext(), wordPLs);
        //RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        //listWords.setLayoutManager(mLayoutManager);
        //listWords.setItemAnimator(new DefaultItemAnimator());
        //listWords.setAdapter(wordAdapter);
    }

    override fun getWordsPL(wordPLs: List<WordPL>) {
        //wordAdapter.setWords(wordPLs);
        //wordAdapter.notifyDataSetChanged();
    }

    override fun getWordsEN(wordENs: List<WordEN>) {
        Log.e("AAAAAAAA", wordENs.size.toString())
        Log.e("AAAAAAAA", wordENs.size.toString())
    }

    override fun initInjection() {

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val fragmentAdapter = FragmentAdapter(fragmentManager as FragmentManager)
        searchViewPager!!.adapter = fragmentAdapter
        mSearchTabLayout!!.setupWithViewPager(searchViewPager)
        mSearchTabLayout!!.removeAllTabs()
        mSearchTabLayout!!.addTab(mSearchTabLayout!!.newTab().setIcon(context!!.resources.getDrawable(R.drawable.poland_flag)))
        mSearchTabLayout!!.addTab(mSearchTabLayout!!.newTab().setIcon(context!!.resources.getDrawable(R.drawable.english_flag)))
//        Log.e("EEEEE", String.valueOf(mSearchTabLayout!!.getTabCount()))
//        drawerManager.inflateToolbar(toolbar, R.layout.toolbar_content_search, activity)
        drawerManager.inflateToolbar(toolbar, R.layout.toolbar_content_search, this.requireActivity())

        if (ContextCompat.checkSelfPermission(activity!!, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(activity!!,
                            Manifest.permission.READ_EXTERNAL_STORAGE)) {
                val alertBuilder = AlertDialog.Builder(activity!!)
                alertBuilder.setCancelable(true)
                alertBuilder.setTitle("Permission necessary")
                alertBuilder.setMessage("External storage permission is necessary")
                alertBuilder.setPositiveButton(android.R.string.yes) { dialog, which ->
                    ActivityCompat.requestPermissions(activity!!,
                            arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE), 23)
                }

                val alert = alertBuilder.create()
                alert.show()
            } else {
                ActivityCompat.requestPermissions(activity!!,
                        arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE), 23)
            }
        } else {
        }
    }

    inner class FragmentAdapter(fm: FragmentManager) : FragmentStatePagerAdapter(fm) {

        var strings = arrayOf("POLSKA", "ANGLIA")

        override fun getPageTitle(position: Int): CharSequence? {
            return strings[position]
        }

        override fun getItem(position: Int): Fragment {
            return SearchDetailsFragment.newInstance(position)
//            return Fragment()
        }

        override fun getCount(): Int {
            return 2
        }
    }
}
