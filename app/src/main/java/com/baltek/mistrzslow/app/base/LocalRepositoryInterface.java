package com.baltek.mistrzslow.app.base;

import android.support.annotation.NonNull;

import com.baltek.mistrzslow.data.Model.IdModel;

import io.reactivex.Observable;
import io.realm.RealmModel;
import io.realm.RealmQuery;
import io.realm.RealmResults;

import java.io.InputStream;
import java.util.List;

import rx.functions.Func1;

interface LocalRepositoryInterface {


    @NonNull
    <T extends RealmModel> Observable<? extends List<T>> getListObservable(Class<T> t,
                                                                           Func1<RealmQuery<T>, RealmResults<T>> filter);

    @NonNull
    <T extends RealmModel> Observable<? extends List<T>> getListObservable(Class<T> t);

    @NonNull
    <T extends RealmModel> Observable<T> getObservable(Class<T> t,
                                                       Func1<RealmQuery<T>, RealmQuery<T>> filter);

    boolean doFileExists(String code);


    <T extends RealmModel> Observable<Boolean> saveData(List<T> list);

    <T extends RealmModel> Observable<Boolean> saveData(T item);

    <E extends RealmModel> Observable<E> getObservable(Class<E> t);


//    Func0<Void> executeTransaction(Func1<Realm, Void> action);

    <T extends RealmModel> Observable<Boolean> deleteData(Class<T> tClass,
                                                          List<IdModel> capture);


    Observable<Boolean> saveAll(InputStream stream);

}
