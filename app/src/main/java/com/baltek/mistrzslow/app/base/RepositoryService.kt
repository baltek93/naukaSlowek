package com.baltek.mistrzslow.app.base

import com.baltek.mistrzslow.data.Model.IdModel
import io.reactivex.Observable
import io.realm.RealmModel
import io.realm.RealmQuery
import io.realm.RealmResults
import rx.functions.Func1
import java.io.InputStream

class RepositoryService(val mLocalRepository: LocalRepository) : Repository() {
    override fun <T : RealmModel?> getListObservable(t: Class<T>?, filter: Func1<RealmQuery<T>, RealmResults<T>>?): Observable<out MutableList<T>> {
        return mLocalRepository.getListObservable(t, filter)
    }

    override fun <T : RealmModel?> getListObservable(t: Class<T>?): Observable<out MutableList<T>> {
        return mLocalRepository.getListObservable(t)

    }

    override fun <T : RealmModel?> getObservable(t: Class<T>?, filter: Func1<RealmQuery<T>, RealmQuery<T>>?): Observable<T> {
        return mLocalRepository.getObservable(t, filter)

    }

    override fun doFileExists(code: String): Boolean {
        return mLocalRepository.doFileExists(code)
    }

    override fun <T : RealmModel?> saveData(list: MutableList<T>?): Observable<Boolean> {
        return mLocalRepository.saveData(list)

    }

    override fun <T : RealmModel?> saveData(item: T): Observable<Boolean> {
        return mLocalRepository.saveData(item)

    }

    override fun <E : RealmModel?> getObservable(t: Class<E>?): Observable<E> {
        return mLocalRepository.getObservable(t)

    }

    override fun <T : RealmModel?> deleteData(tClass: Class<T>?, idModels: MutableList<IdModel>?): Observable<Boolean> {
        return if (idModels == null || idModels.size == 0) Observable.just(true) else mLocalRepository.deleteData(tClass, idModels)

    }

    override fun  saveAll(stream: InputStream): Observable<Boolean> {
        return mLocalRepository.saveAll(stream = stream)

    }
}